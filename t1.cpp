#include <iostream>
#include <string>
#include <math.h>

using namespace std;

const uint CHARACTERS_COUNT = 127;
uint asciiArray[CHARACTERS_COUNT];


uint charToUint(char charNum) {
    if (charNum < 0 || charNum >= CHARACTERS_COUNT)
        return 0;
    uint digit = asciiArray[charNum];
    return digit;
}
uint stringToUint(string str) {
    int length = str.length();

    uint numbers[length];
    uint result = 0;
    for (int i = length-1; i >= 0; i--) {
        uint digit = charToUint(str[i]);
        // printf("digit: %d\n",digit);
        uint power = pow(10, length-i-1);
        uint numberValue = digit * power; 
        numbers[i] = numberValue;
        // cout << "i:" << i << "\n";
        // cout << "pow: " << power << "\n";
        cout << digit << " = " << numberValue << "\n";
        result += numberValue;
    }
    for (int i = 0; i < length; i++) {
        if (i == 0) {
            cout << "" << numbers[i];
        }else {

            cout << " + " << numbers[i];
        }
    }
    cout << "\n = " << result << "\n";
    
    return result;
}

int main() {
    // Map ascii numbers to real numbers
    for (int i = 0; i < CHARACTERS_COUNT; i++) asciiArray[i] = 0;
    for (int i = 0; i < 10; i++) {
        asciiArray[48+i] = i;
    }
    char txt[] = "52";
    for (int i = 0; i < 2; i++) {
       
    }

    string input;
    cout << "Enter numbers: ";
    cin >> input;
    int inputAsInt = stringToUint(input);
    // cout << "result: " << inputAsInt << "\n";
    return 0;
}