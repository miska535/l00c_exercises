#include <iostream>
#include <vector> 
#include <bitset>
using namespace std;

const int lastNumber = 35;
const size_t bitVectorSize = lastNumber + 1;

bool isPrimeNumber(uint number) {
    for (int i = 2; i < number-1; i++) {
        if (number % i == 0) return false;
    }
    return true;
}
 bitset<bitVectorSize> getPrimeNumbersTo(int endNumber) {
    bitset<bitVectorSize> bits;
    
    bits.set(0, false);
    bits.set(1, false);
    for (uint i = 2; i <= endNumber; i++) {
        if (isPrimeNumber(i)) bits.set(i, true);
    }
    return bits;
}

int main() {
    auto primes = getPrimeNumbersTo(lastNumber);
    cout << "Prime numbers from 0 to " << lastNumber << " as a bit vector:\n";
    cout << primes.to_string() << "\n";
    return 0;
}