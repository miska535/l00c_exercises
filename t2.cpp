#include <iostream>

using namespace std;

int main() {
    uint number = 16;

    bitset<8> bits(number);
    cout << "unsigned integer '" << number 
    << "' in binary format: " << bits.to_string();
    // printf("%s", bits.to_string().c_str());
    return 0;
}