#include <iostream>
#include <vector> 
using namespace std;

const int lastNumber = 50;

bool isPrimeNumber(uint number) {
    for (int i = 2; i < number-1; i++) {
        if (number % i == 0) return false;
    }
    return true;
}
vector<uint> getPrimeNumbersTo(int endNumber) {
    vector<uint> primeNumbers;
    for (uint i = 2; i <= endNumber; i++) {
        if (isPrimeNumber(i)) primeNumbers.push_back(i);
    }
    return primeNumbers;
}

int main() {
    auto primes = getPrimeNumbersTo(lastNumber);
    cout << "Prime numbers until " << lastNumber << ":\n";
    for (auto it = primes.begin(); it != primes.end(); ++it) {
        // printf("%d\n",*it);
        cout << "" << *it << "\n";
    }

    return 0;
}